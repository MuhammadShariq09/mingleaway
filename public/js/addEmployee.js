function validateAddEmployee() {

    if ($('input[name="name"]').val() == '') {
        toastr.error("Employee name can not be empty", 'Error !');
        return false;
    }
    if ($("#employee_id").val() == '') {
        if ($('input[name="personal_img"]').val() == '') {
            toastr.error("Employee profile picture is required", 'Error !');
            return false;
        }
        if ($('input[name="email"]').val() == '') {
            toastr.error("Employee email can not be emtpy", 'Error !');
            return false;
        }
        if ($('input[name="password"]').val() == '') {
            toastr.error("Employee password can not be emtpy", 'Error !');
            return false;
        }
    }
    if ($('input[name="availablity_hour_from"]').val() == '') {
        toastr.error("Employee availabity hours are required", 'Error !');
        return false;
    }
    if ($('input[name="availablity_hour_to"]').val() == '') {
        toastr.error("Employee availabity hours are required", 'Error !');
        return false;
    }
    if ($('input[name="number"]').val() == '') {
        toastr.error("Employee number can not be emtpy", 'Error !');
        return false;
    }

    return true;
}


function removePackage(id) {
    $.ajax({
        url: base_url + '/employee/remove-current-package/' + id,
        success: function(response) {
            getEmployeePackage();
        }
    })
}


function removeImage(id) {
    toastr.warning("<br /><button type='button' value='yes'>Yes</button><button type='button'  value='no' >No</button>", 'Are you sure you want to delete this Image?', {
        allowHtml: true,
        onclick: function(toast) {
            value = toast.target.value
            if (value == 'yes') {
                toastr.remove();
                $.ajax({
                    url: base_url + '/employee/delete-gallary-image/' + id,
                    success: function(response) {
                        $(".removeEmpGal" + id).remove();
                    },
                    error: function(response) {
                        console.log(response);
                    }
                })
            } else {
                toastr.remove();
            }
        }

    })
}