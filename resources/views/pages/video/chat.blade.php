@extends('layouts.app')
@section('title','Video Chat')
@section('body-class','vertical-layout vertical-menu 2-columns fixed-navbar pace-done menu-expanded')
@section('css')
@endsection
@section('content')

<div class="app-content content">
   <div class="content-wrapper">
      <div class="content-body">
         <section id="configuration">
            <div class="row">
               <div class="col-12">
                  <div class="card rounded">
                     <div class="card-content collapse show">
                       <iframe width="100%" allow="camera;microphone" height="735px" src="http://localhost/test/demos/?user_id={{$token}}"></iframe>
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </div>
   </div>
</div>

@section('js')
@endsection
@endsection
