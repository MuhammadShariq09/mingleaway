@extends('layouts.app')
@section('title','Employee')
@section('body-class','vertical-layout vertical-menu 2-columns fixed-navbar pace-done menu-expanded')
@section('css')
@endsection
@section('content')

<div class="app-content content">
    <div class="content-wrapper">
      <div class="content-body">

        @if(Session::has('message'))
          <div class="alert alert-success">
            <strong>{{ Session::get('message')  }}</strong>
          </div>
        @endif
        @if(Session::has('error'))
          <div class="alert alert-danger">
            <strong>{{ Session::get('error')  }}</strong>
          </div>
        @endif


        <!-- Basic form layout section start -->
         <section id="configuration">
          <div class="row">
            <div class="col-12">
              <div class="card rounded">
                <div class="card-content collapse show">
                     <div class="card-body card-dashboard">

                     <h1 class="pull-left">Employee</h1>
                     <a href="{{route('add-employee')}}" class="green-btn-project"><i class="fa fa-plus-circle"></i> Add Employe</a>

                     <div class="maain-tabble">
                      <table class="table table-striped table-bordered zero-configuration">
                      <thead>
                        <tr>
                          <th>Employee Name</th>
                          <th>Email Address</th>
                          <th>Phone</th>
                          <th>Available Hrs</th>
                         </tr>
                      </thead>
                      <tbody>
                        @foreach ($employees as $key => $emp)
                       <tr id="row{{$emp->id}}">
                          <td><img src="{{asset($emp->personal_img)}}" class="tab-img" alt=""> {{$emp->name}}</td>
                          <td><i class="fa fa-envelope" aria-hidden="true"></i><a>{{$emp->email}}</a></td>
                          <td>{{$emp->phone}}</td>

                          <td>{{$emp->avail_hrs_from.' to '.$emp->avail_hrs_to}}</td>
                          <td>
                          <div class="btn-group mr-1 mb-1">
                            <button type="button" class="btn dropdown-toggle btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></button>
                            <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 21px, 0px); top: 0px; left: 0px; will-change: transform;">
                            <a class="dropdown-item" href="{{url('/employee/edit/'.$emp->id)}}"><i class="fa fa-pencil-square-o"></i>Edit</a>
                            <a class="dropdown-item" onclick="deleteEmployee({{$emp->id}})" href="#!"><i class="fa fa-eye-slash"></i>Delete</a>
                            </div>
                          </div>
                        </td>
                        </tr>
                        @endforeach

                      </tbody>
                    </table>
                    </div>
                    </div><!--card body end-->
                </div>
              </div>
            </div>
          </div>
        </section>
        <!-- // Basic form layout section end -->
      </div>
    </div>
  </div>


@section('js')
<script type="text/javascript">

function deleteEmployee(id) {
  toastr.warning("<br /><button type='button' value='yes'>Yes</button><button type='button'  value='no' >No</button>", 'Are you sure you want to delete this ?', {
      allowHtml: true,
      onclick: function(toast) {
          value = toast.target.value
          if (value == 'yes') {
              toastr.remove();
              $.ajax({
                  url: base_url + '/employee/delete-employee/' + id,
                  success: function(response) {
                      $("#row" + id).remove();
                  },
                  error: function(response) {
                      console.log(response);
                  }
              })
          } else {
              toastr.remove();
          }
      }

  })

}

</script>

@endsection


@endsection
