@extends('layouts.app')
@section('title','Employee Profile')
@section('body-class','vertical-layout vertical-menu 2-columns fixed-navbar pace-done menu-expanded')
@section('css')

@section('content')
<div class="app-content content">
   <div class="content-wrapper">
      <div class="content-body">
         <!-- Basic form layout section start -->
         <section id="configuration">
            <div class="row">
               <div class="col-12">
                  <div class="card">
                     <div class="card-content collapse show">
                        <div class="card-body card-dashboard client-pro-main">
                           <h1>Employee Profile</h1>
                           <div class="row">
                              <div class="col-md-2 col-sm-12">
                                 <img class="empavatar" src="{{asset($meta->personal_img)}}" class="pro-img" alt="" />
                              </div>
                              <!--left profile image col end-->
                              <div class="col-md-8 col-sm-12">
                                 <h2>{{$employee->name}}</h2>
                                 <p>{{$meta->aboutsus}}
                                 </p>
                                 <div class="row">
                                    @foreach ($images as $key => $img)
                                      <div class="col-md-2 col-sm-12">
                                        <img src="{{asset($img->path)}}" class="emplo-img-glr" alt="" />
                                      </div>
                                    @endforeach
                                 </div>
                                 <!--images row end-->
                                 <div class="row employ-icn-roww">
                                    <div class="col-md-4 col-sm-12">
                                       <p><i class="fa fa-transgender"></i>Gender <span class="client-innr-span">{{$meta->gender}}</span></p>
                                       <p><i class="fa fa-mars-double"></i>Heritage <span class="client-innr-span">{{$meta->heritage}}</span></p>
                                       <p><i class="fa fa-paint-brush"></i>Hair Color <span class="client-innr-span">{{$meta->haircolor}}</span></p>
                                    </div>
                                    <div class="col-md-4 col-sm-12">
                                       <p><i class="fa fa-phone"></i>Phone Number <span class="client-innr-span">{{$meta->phone}}</span></p>
                                       <p><i class="fa fa-clock-o"></i>Availblity <span class="client-innr-span">{{$meta->avail_hrs_from.' to '.$meta->avail_hrs_to}}</span></p>
                                       <p><i class="fa fa-globe"></i>Status <span class="client-innr-span">Online</span></p>
                                    </div>
                                 </div>
                                 <!--row end-->
                              </div>
                              <!--right col end-->
                              <div class="col-md-2 col-sm-12"></div>
                           </div>
                           <!--row end-->
                           <div class="cntr-btnn-main">
                              <button type="button" data-toggle="modal" data-target="#default2">Book Now</button>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <!-- // Basic form layout section end -->
      </div>
   </div>
</div>
<!-- ////////////////////////////////////////////////////////////////////////////-->
<div class="modal fade text-left show" id="default2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1">
   <div class="modal-dialog add-clint-modaal user-pack-popup-main" role="document">
      <div class="modal-content">
         <div class="modal-body">
            <div class="gift-popup">
               <h5>packages</h5>
               <div class="row">
                 @foreach($package as $p)
                  <div class="col-md-4 col-sm-12">
                     <div class="user-pack-popup-main-boxs">
                        <h6>{{$p->package_name}}</h6>
                        <ul>
                           <li><strong>Time: </strong> {{$p->package_time}} Minutes </li>
                           <li><strong>Cost: </strong> {{$p->package_cost}} $</li>
                        </ul>
                        <a onclick="subscribePackage('{{$p->id}}')" href="#!">Subscribe</a>
                     </div>
                  </div>
                  @endforeach
               </div>
            </div>
         </div>
         <!--invice modal end-->
      </div>
   </div>
</div>
@section('js')
<script type="text/javascript">
  function subscribePackage(package){
      $.ajax({
        url: base_url + '/customer/subscribe-package',
        type: 'post',
        data: { package: package },
        success: function(response){
            if(response.status == 200){
              toastr.success("Package is subscribed redirecting....","Success");
              setTimeout(function() {
                location.replace(response.url);
              }, 2000);
            }
        }
      })
  }
</script>
@endsection
@endsection
