@extends('layouts.app')
@section('title','Edit Employee')
@section('body-class','vertical-layout vertical-menu 2-columns fixed-navbar pace-done menu-expanded')
@section('css')
<link rel="stylesheet" href="{{asset('app-assets/css/plugins/forms/wizard.css')}}">

@endsection
@section('content')

<div class="app-content content">
   <div class="content-wrapper">
      <div class="content-body">
         <!-- Basic form layout section start -->
         <section id="configuration">
            <div class="row">
               <div class="col-12">
                  <div class="card rounded">
                     <div class="card-content collapse show">
                        <div class="card-body card-dashboard">
                           <h1>Add Employee</h1>
                           <div class="admin-employ-add">
                              <section id="number-tabs">
                                 <div class="row">
                                    <div class="col-12">
                                       <div class="card">
                                          <div class="card-content collapse show">
                                             <div class="card-body">
                                                <form id="addEmployee" enctype="multipart/form-data" action="{{route('updateEmployee')}}" method="post" class="number-tab-steps wizard-circle">
                                                   <!-- Step 1 -->
                                                   @csrf
                                                   <input type="hidden" name="employee_id" id="employee_id" value="{{$employee->id}}">
                                                   <h6>Step</h6>
                                                   <fieldset>
                                                      <div class="form-body">
                                                         <h2>Add Personal</h2>
                                                         <div class="row">
                                                            <div class="col-md-6 col-sm-12">
                                                               <div class="row">
                                                                  <div class="col-md-8 col-sm-12">
                                                                     <div class="form-group">
                                                                        <label for="timesheetinput1">Employee Name *</label>
                                                                        <div class="position-relative has-icon-left">
                                                                           <input type="text" id="timesheetinput1" value="{{$employee->name}}" required class="form-control" name="name">
                                                                           <div class="form-control-position"> <i class="fa fa-user"></i> </div>
                                                                        </div>
                                                                     </div>
                                                                     <div class="form-group">
                                                                        <label for="timesheetinput1">Gender</label>
                                                                        <div>
                                                                           <div class="d-inline-block custom-control custom-radio mr-1">
                                                                              <input type="radio" {{$meta->gender == 'male' ? 'checked' : ''}} value="male" required class="custom-control-input" name="gender" id="radio1">
                                                                              <label class="custom-control-label" for="radio1">Male</label>
                                                                           </div>
                                                                           <div class="d-inline-block custom-control custom-radio mr-1">
                                                                              <input type="radio" {{$meta->gender == 'female' ? 'checked' : ''}} required value="female" class="custom-control-input" name="gender" id="radio2">
                                                                              <label class="custom-control-label" for="radio2" checked="">Female</label>
                                                                           </div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                                  <!--inner col end-->
                                                                  <div class="col-md-4 col-sm-12">
                                                                     <button type="button" name="file" class="uplon-btn" onclick="document.getElementById('upload').click()">
                                                                     <img src="{{ $meta->personal_img ?? '/images/upload-img.jpg' }}" id="personal_img" class="img-full" alt=""></button>
                                                                     <input type="file" accept="image/*" required onchange="readURL(this, 'personal_img')" id="upload" name="personal_img">
                                                                  </div>
                                                                  <!--inner col end-->
                                                               </div>
                                                               <!--inner row end-->
                                                            </div>
                                                            <!--col end-->
                                                            <div class="col-md-6 col-sm-12">
                                                              <div class="form-group">
                                                                 <label for="timesheetinput1">Address *</label>
                                                                 <div class="position-relative has-icon-left">
                                                                    <input type="text" value="{{$meta->address}}" id="autocomplete" required onfocus="geolocate()" class="form-control" name="address" />
                                                                    <div class="form-control-position"> <i class="fa fa-map-marker"></i> </div>
                                                                 </div>
                                                              </div>
                                                              <div class="form-group">
                                                                 <label for="timesheetinput1">Country *</label>
                                                                 <div class="position-relative has-icon-left">
                                                                    <input type="text" value="{{$meta->country}}" required class="form-control" id="country" name="country" />
                                                                    <div class="form-control-position"> <i class="fa fa-globe"></i> </div>
                                                                 </div>
                                                              </div>
                                                            </div>
                                                            <!--col end-->
                                                         </div>
                                                         <!--row end-->
                                                         <div class="row">
                                                            <div class="col-md-6 col-sm-12">
                                                               <div class="form-group">
                                                                  <label for="timesheetinput1">Stage Name</label>
                                                                  <div class="position-relative has-icon-left">
                                                                     <input type="text" value="{{$meta->stage_name}}" required class="form-control" name="stagename" />
                                                                     <div class="form-control-position"> <i class="fa fa-check-circle"></i> </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <!--col end-->
                                                            <div class="col-md-6 col-sm-12">
                                                              <div class="form-group">
                                                                 <label for="timesheetinput1">State *</label>
                                                                 <div class="position-relative has-icon-left">
                                                                    <input type="text" value="{{$meta->state}}" required class="form-control" name="state" id="administrative_area_level_1" />
                                                                    <div class="form-control-position"> <i class="fa fa-globe"></i> </div>
                                                                 </div>
                                                              </div>
                                                            </div>
                                                            <!--col end-->
                                                         </div>
                                                         <!--row end-->
                                                         <div class="row">
                                                            <div class="col-md-6 col-sm-12">
                                                               <div class="form-group">
                                                                  <label for="timesheetinput1">Consultant ID</label>
                                                                  <div class="position-relative has-icon-left">
                                                                     <input type="text" value="{{$meta->consultant_id}}" required class="form-control" name="consultant_id" />
                                                                     <div class="form-control-position"> <i class="fa fa-id-card"></i> </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <!--col end-->
                                                            <div class="col-md-6 col-sm-12">
                                                              <div class="form-group">
                                                                 <label for="timesheetinput1">City *</label>
                                                                 <div class="position-relative has-icon-left">
                                                                    <input type="text" value="{{$meta->city}}" required class="form-control" id="locality" name="city" />
                                                                    <div class="form-control-position"> <i class="fa fa-globe"></i> </div>
                                                                 </div>
                                                              </div>
                                                            </div>
                                                            <!--col end-->
                                                         </div>
                                                         <!--row end-->
                                                         <div class="row">
                                                           <div class="col-md-6 col-sm-12">
                                                              <div class="form-group">
                                                                 <label for="timesheetinput1">Update Resume (@php  echo ($meta->resume) ?  '<a href="'.asset($meta->resume).'">Resume</a>' : '' @endphp)</label>
                                                                 <div class="position-relative has-icon-left">
                                                                    <input required accept="image/*" type="file" name="resume" />
                                                                 </div>
                                                              </div>
                                                           </div>
                                                            <!--col end-->
                                                            <div class="col-md-6 col-sm-12">
                                                              <div class="form-group">
                                                                 <label for="timesheetinput1">ZipCode *</label>
                                                                 <div class="position-relative has-icon-left">
                                                                    <input required type="text" value="{{$meta->zipcode}}"  class="form-control" id="postal_code" name="postal_code" />
                                                                    <div class="form-control-position"> <i class="fa fa-globe"></i> </div>
                                                                 </div>
                                                              </div>
                                                            </div>
                                                            <!--col end-->
                                                         </div>
                                                         <!--row end-->
                                                         <div class="row">
                                                            <div class="col-md-6 col-sm-12">
                                                              <div class="form-group">
                                                                 <label for="timesheetinput1">Email Address *</label>
                                                                 <div class="position-relative has-icon-left">
                                                                    <input required type="email" value="{{$employee->email}}"  class="form-control" disabled name="email" />
                                                                    <div class="form-control-position"> <i class="fa fa-envelope"></i> </div>
                                                                 </div>
                                                              </div>
                                                            </div>
                                                            <!--col end-->
                                                            <div class="row col-md-6 col-sm-12">
                                                              <div class="col-md-6">
                                                               <div class="form-group">
                                                                 <div class="form-group">
                                                                    <label for="timesheetinput1">Availblity Hours From  *</label>
                                                                    <div class="position-relative has-icon-left">
                                                                      <input required type='text' value="{{$meta->avail_hrs_from}}"  name="availablity_hour_from" class="form-control pickatime" placeholder="Basic Pick-a-time" />
                                                                       <div class="form-control-position"> <i class="fa fa-clock-o"></i> </div>
                                                                    </div>
                                                                 </div>
                                                               </div>
                                                             </div>
                                                              <div class="col-md-6">
                                                                <div class="form-group">
                                                                   <label for="timesheetinput1">Availblity Hours To:  *</label>
                                                                   <div class="position-relative has-icon-left">
                                                                     <input required type='text' value="{{$meta->avail_hrs_to}}" name="availablity_hour_to" class="form-control pickatime" placeholder="Basic Pick-a-time" />
                                                                      <div class="form-control-position"> <i class="fa fa-clock-o"></i> </div>
                                                                   </div>
                                                                </div>

                                                              </div>

                                                            <!--col end-->
                                                         </div>
                                                         </div>
                                                         <!--row end-->
                                                         <div class="row">
                                                            <div class="col-md-6 col-sm-12">
                                                              <div class="form-group">
                                                                 <label for="timesheetinput1">Update Password or leave Blank</label>
                                                                 <div class="position-relative has-icon-left">
                                                                    <input required type="password" class="form-control" name="password" />
                                                                    <div class="form-control-position"> <i class="fa fa-key"></i> </div>
                                                                 </div>
                                                              </div>
                                                            </div>
                                                            <!--col end-->
                                                            <div class="col-md-6 col-sm-12">
                                                                <div class="form-group">
                                                                   <label for="timesheetinput1">Phone Number *</label>
                                                                   <div class="position-relative has-icon-left">
                                                                      <input required value="{{$meta->phone}}" type="number" class="form-control" name="number" />
                                                                      <div class="form-control-position"> <i class="fa fa-key"></i> </div>
                                                                   </div>
                                                                </div>
                                                            </div>
                                                            <!--col end-->
                                                         </div>
                                                         <div class="row">
                                                            <div class="col-md-6 col-sm-12">
                                                                 <div class="form-group">
                                                                    <label for="timesheetinput1">Hair Color</label>
                                                                    <div class="position-relative has-icon-left">
                                                                      <input type="text" value="{{$meta->haircolor}}" class="form-control" name="haircolor" />
                                                                       <div class="form-control-position"> <i class="fa fa-circle"></i> </div>
                                                                    </div>
                                                                 </div>
                                                            </div>
                                                            <!--col end-->
                                                            <div class="col-md-6">
                                                              <div class="form-group">
                                                                 <label for="timesheetinput1">Heritage</label>
                                                                 <div class="position-relative has-icon-left">
                                                                    <input required type="text" value="{{$meta->heritage}}" class="form-control" name="heritage" />
                                                                    <div class="form-control-position"> <i class="fa fa-key"></i> </div>
                                                                 </div>
                                                              </div>
                                                            </div>
                                                            <!--col end-->
                                                         </div>
                                                         <!--row end-->
                                                      </div>
                                                      <!--form body end-->
                                                   </fieldset>
                                                   <!-- Step 2 -->
                                                   <h6>Step 2</h6>
                                                   <fieldset>
                                                     <div class="row">
                                                        @foreach($images as $img)
                                                        <div class="col-md-2 col-sm-12">
                                                           <button onclick="removeImage({{$img->id}})" type="button" name="file" class="removeEmpGal{{$img->id}} uplon-btn2" onclick="">
                                                              <img src="{{asset($img->path)}}" class="emplo-img-glr" alt="" />
                                                              <span  class="removeEmpGal">X</span>
                                                           </button>
                                                        </div>
                                                        @endforeach
                                                     </div>
                                                      <div id="gallery" class="row gallery">
                                                         <div id="gallery-title" class="gallery-title col-md-12">
                                                            <h2>Add Photos</h2>
                                                         </div>
                                                         <div id="upload-gallery" class="upload-gallery col-md-2 col-sm-12">
                                                            <button type="button" name="file" class="uplon-btn2" onclick="document.getElementById('gallery-photo-add').click()">
                                                            <img src="/images/Add-Employee-Step-2_24.png" class="emplo-img-glr" alt="" />
                                                            </button>
                                                            <input type="file" accept="image/*" class="gallery-photo-add" id="gallery-photo-add" multiple name="gallary_photos[]">
                                                         </div>
                                                      </div>
                                                      <h2>About Us  <span class="emply-step2-span">(optional)</span></h2>
                                                      <textarea name="aboutus" class="form-control" rows="8" cols="80">{{$meta->aboutsus}}</textarea>
                                                   </fieldset>
                                                   <!-- Step 3 -->
                                                   <h6>Step 3</h6>
                                                   <fieldset>
                                                      <h2>Add Package Details</h2>
                                                      <div class="row" id="currentpakg"></div>
                                                      <hr>
                                                      <!--row end-->
                                                      <div class="clearfix"></div>
                                                      <div class="row">
                                                         <div class="col-md-3 col-sm-12">
                                                            <div class="form-group">
                                                               <label for="package_name">Package Name</label>
                                                               <input type="text" class="form-control" placeholder="Enter Package name" id="package_name">
                                                            </div>
                                                         </div>
                                                         <div class="col-md-3 col-sm-12">
                                                            <div class="form-group">
                                                               <label for="package_time">Time</label>
                                                               <input type="text" class="form-control" placeholder="Enter Time in minutes" id="package_time">
                                                            </div>
                                                         </div>
                                                         <div class="col-md-3 col-sm-12">
                                                            <div class="form-group">
                                                               <label for="package_cost">Cost</label>
                                                               <input type="text" placeholder="Enter Package Cost" class="form-control" id="package_cost">
                                                            </div>
                                                         </div>
                                                          <div class="col-md-3">
                                                            <a href="#!" class="addPackg emply-step3-packges-btn pull-right"><i class="fa fa-plus-circle"></i> Add Package</a>
                                                          </div>
                                                      </div>

                                                      <!--row end-->
                                                   </fieldset>
                                                   <!-- Step 4 -->
                                                   <h6>Step 4</h6>
                                                   <fieldset>
                                                      <br>
                                                      <div class="row">
                                                         <div class="col-md-3 col-sm-12"></div>
                                                         <div class="col-md-6 col-sm-12">
                                                            <div class="form-group">
                                                               <label for="meetingName1">Account Name</label>
                                                               <input type="text" value="{{$meta->accountname}}" name="account_name" class="form-control" />
                                                            </div>
                                                            <div class="form-group">
                                                               <label for="meetingName1">Account Number</label>
                                                               <input type="text" value="{{$meta->accountnumber}}" name="account_number" class="form-control" />
                                                            </div>
                                                            <div class="form-group">
                                                               <label for="meetingName1">CVC Number</label>
                                                               <input type="text" value="{{$meta->cvcnumber}}" name="account_cvc" class="form-control" />
                                                            </div>
                                                            <div class="form-group">
                                                               <label for="meetingName1">Expiry Date</label>
                                                               <input type="text" value="{{$meta->expirynumber}}" name="account_expdate" class="form-control" />
                                                            </div>
                                                         </div>
                                                         <div class="col-md-3 col-sm-12"></div>
                                                      </div>
                                                      <!--rpw end-->
                                                   </fieldset>
                                                </form>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </section>
                           </div>
                           <!--admin employ add end-->
                        </div>
                        <!--card body end-->
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <!-- // Basic form layout section end -->
      </div>
   </div>
</div>
<!-- ////////////////////////////////////////////////////////////////////////////-->





@section('js')
<script type="text/javascript" src="{{asset('/js/addEmployee.js')}}"></script>
<script type="text/javascript">
  function readURL(input, target) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function(e) {
        $('#'+target).attr('src', e.target.result);
      }
      reader.readAsDataURL(input.files[0]);
    }
  }

  $('.gallery-photo-add').on('change', function() {
      var myNode = document.getElementById("gallery");
      while (myNode.lastChild.id !== 'upload-gallery') {
          myNode.removeChild(myNode.lastChild);
      }
      imagesPreview(this, 'div.gallery');
  });

  function imagesPreview(input, placeToInsertImagePreview) {
    if (input.files) {
        var filesAmount = input.files.length;
        for (i = 0; i < filesAmount; i++) {
            var reader = new FileReader();
            reader.onload = function(event) {
              var str = `<div class="col-md-2 col-sm-12">
                          <img src="`+event.target.result+`" class="emplo-img-glr" alt="" />
                         </div>`;
              $(".gallery").append(str);
              // str.appendTo(placeToInsertImagePreview);
            }
            reader.readAsDataURL(input.files[i]);
        }
    }
  }

  $(document).ready(function(){
    getEmployeePackage();
  })

  $(".addPackg").on('click', function(){
    if( $('#package_name').val() == '' || $('#package_time').val() == '' || $('#package_cost').val() == '' ){
      toastr.error("Can't add Package, Please fill the required fields","Error");
    }
    else{
      var post = new Object();
      post.package_name = $("#package_name").val();
      post.package_time = $("#package_time").val();
      post.package_cost = $("#package_cost").val();
      $.ajax({
        url: base_url+'/employee/add-employee-package',
        data: post,
        type: 'post',
        success: function(response){
          if(response.status == 200){
            toastr.success(response.msg,"Success");
            $('#package_name').val('');
            $('#package_time').val('');
            $('#package_cost').val('');
            getEmployeePackage();
          }
          else
            toastr.error(response.msg,"Error");
        }
      })
    }

  })

  function getEmployeePackage(){
      var str='';
      $.ajax({
        url : base_url + '/employee/get-current-employee-package',
        success: function(response){
          response.map(function(data){
            str += `<div class="col-md-4 col-sm-12">
               <div class="emply-step3-packges">
                  <h5>`+data.package_name+`
                    <a onclick="removePackage('`+data.id+`')" data-id="`+data.id+`" class="pull-right rmvpkg pr-2">X</a>
                  </h5>
                  <div class="row">
                     <div class="col-md-6">
                        <p>Time: `+data.package_time+` Mins</p>
                     </div>
                     <div class="col-md-6">
                        <p>Cost: $`+data.package_cost+`</p>
                     </div>
                  </div>
               </div>
            </div>
            `;
          });

          $("#currentpakg").html(str);
        }
      })
  }



</script>
@endsection


@endsection
