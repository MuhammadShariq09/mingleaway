@extends('layouts.app')
@section('title','List of Employees')
@section('body-class','vertical-layout vertical-menu 2-columns fixed-navbar pace-done menu-expanded')
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/plugins/calendars/fullcalendar.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/calendars/fullcalendar.min.css')}}">
@endsection
@section('content')

<div class="app-content content">
   <div class="content-wrapper">
      <div class="content-body">
         <!-- Basic form layout section start -->
         <section id="configuration">
            <div class="row">
               <div class="col-12">
                  <div class="card">
                     <div class="card-content collapse show">
                        <div class="card-body card-dashboard client-pro-main">
                           <h1>Employees Listing</h1>
                           <div class="row">
                              <div class="col-md-9 col-sm-12">
                                 <div class="row">
                                   @foreach ($employees as $key => $emp)
                                     <div class="col-md-4 col-sm-12 user-employe-listing">
                                       <img class="empavatar" src="{{asset($emp->personal_img)}}" alt="" />
                                       <h3>{{$emp->name}}</h3>
                                       <p><i class="fa fa-transgender"></i>Gender <span class="client-innr-span">{{$emp->gender}}</span></p>
                                       <p><i class="fa fa-calendar"></i>Availability <span class="client-innr-span">{{$emp->avail_hrs_from.' to '.$emp->avail_hrs_to}}</span></p>
                                       <a href="{{url('/employee/info/'.$emp->id)}}" class="user-employe-listing-veiw-btn"><i class="fa fa-eye"></i> View</a>
                                       <a href="#" class="user-employe-listing-veiw-btn"><i class="fa fa-calendar-plus-o"></i>Book Now</a>
                                     </div>
                                   @endforeach

                                 </div>
                                 <!--row end-->
                              </div>
                              <!--left row end-->
                              <div class="col-md-3 col-sm-12">
                                 <h3 class="emply-overview-h3">Top 10 Employee</h3>
                                 <div class="emply-overview-card clearfix">
                                    <div class="row">
                                       <div class="col-md-8">
                                          <img src="/images/client-profile-img.jpg" alt="">
                                          <p>Rob Hopkins</p>
                                          <span><i class="fa fa-transgender"></i> Male</span>
                                          <span><i class="fa fa-calendar"></i> 05-07-2018</span>
                                       </div>
                                       <div class="col-md-4">
                                          <a href="#" class="user-employe-listing-veiw-btn"><i class="fa fa-eye"></i> View</a>
                                          <a href="#" class="user-employe-listing-veiw-btn"><i class="fa fa-calendar-plus-o"></i>Book Now</a>
                                       </div>
                                    </div>
                                    <!--row end-->
                                 </div>
                                 <div class="emply-overview-card clearfix">
                                    <div class="row">
                                       <div class="col-md-8">
                                          <img src="/images/client-profile-img.jpg" alt="">
                                          <p>Rob Hopkins</p>
                                          <span><i class="fa fa-transgender"></i> Male</span>
                                          <span><i class="fa fa-calendar"></i> 05-07-2018</span>
                                       </div>
                                       <div class="col-md-4">
                                          <a href="#" class="user-employe-listing-veiw-btn"><i class="fa fa-eye"></i> View</a>
                                          <a href="#" class="user-employe-listing-veiw-btn"><i class="fa fa-calendar-plus-o"></i>Book Now</a>
                                       </div>
                                    </div>
                                    <!--row end-->
                                 </div>
                                 <div class="emply-overview-card clearfix">
                                    <div class="row">
                                       <div class="col-md-8">
                                          <img src="/images/client-profile-img.jpg" alt="">
                                          <p>Rob Hopkins</p>
                                          <span><i class="fa fa-transgender"></i> Male</span>
                                          <span><i class="fa fa-calendar"></i> 05-07-2018</span>
                                       </div>
                                       <div class="col-md-4">
                                          <a href="#" class="user-employe-listing-veiw-btn"><i class="fa fa-eye"></i> View</a>
                                          <a href="#" class="user-employe-listing-veiw-btn"><i class="fa fa-calendar-plus-o"></i>Book Now</a>
                                       </div>
                                    </div>
                                    <!--row end-->
                                 </div>
                                 <div class="emply-overview-card clearfix">
                                    <div class="row">
                                       <div class="col-md-8">
                                          <img src="/images/client-profile-img.jpg" alt="">
                                          <p>Rob Hopkins</p>
                                          <span><i class="fa fa-transgender"></i> Male</span>
                                          <span><i class="fa fa-calendar"></i> 05-07-2018</span>
                                       </div>
                                       <div class="col-md-4">
                                          <a href="#" class="user-employe-listing-veiw-btn"><i class="fa fa-eye"></i> View</a>
                                          <a href="#" class="user-employe-listing-veiw-btn"><i class="fa fa-calendar-plus-o"></i>Book Now</a>
                                       </div>
                                    </div>
                                    <!--row end-->
                                 </div>
                                 <div class="emply-overview-card clearfix">
                                    <div class="row">
                                       <div class="col-md-8">
                                          <img src="/images/client-profile-img.jpg" alt="">
                                          <p>Rob Hopkins</p>
                                          <span><i class="fa fa-transgender"></i> Male</span>
                                          <span><i class="fa fa-calendar"></i> 05-07-2018</span>
                                       </div>
                                       <div class="col-md-4">
                                          <a href="#" class="user-employe-listing-veiw-btn"><i class="fa fa-eye"></i> View</a>
                                          <a href="#" class="user-employe-listing-veiw-btn"><i class="fa fa-calendar-plus-o"></i>Book Now</a>
                                       </div>
                                    </div>
                                    <!--row end-->
                                 </div>
                                 <div class="emply-overview-card clearfix">
                                    <div class="row">
                                       <div class="col-md-8">
                                          <img src="/images/client-profile-img.jpg" alt="">
                                          <p>Rob Hopkins</p>
                                          <span><i class="fa fa-transgender"></i> Male</span>
                                          <span><i class="fa fa-calendar"></i> 05-07-2018</span>
                                       </div>
                                       <div class="col-md-4">
                                          <a href="#" class="user-employe-listing-veiw-btn"><i class="fa fa-eye"></i> View</a>
                                          <a href="#" class="user-employe-listing-veiw-btn"><i class="fa fa-calendar-plus-o"></i>Book Now</a>
                                       </div>
                                    </div>
                                    <!--row end-->
                                 </div>
                              </div>
                              <!--right col end-->
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <!-- // Basic form layout section end -->
      </div>
   </div>
</div>
@section('js')
<script src="{{asset('app-assets/vendors/js/extensions/fullcalendar.min.js')}}" type="text/javascript"></script>
<script src="{{asset('app-assets/js/scripts/extensions/fullcalendar.js')}}" type="text/javascript"></script>

@endsection
@endsection
