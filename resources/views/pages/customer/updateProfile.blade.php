@extends('layouts.app')
@section('title','Update Profile')
@section('body-class','vertical-layout vertical-menu 2-columns fixed-navbar pace-done menu-expanded')
@section('css')
@endsection
@section('content')

<div class="app-content content">
   <div class="content-wrapper">
      <div class="content-body">

                @if(Session::has('message'))
                  <div class="alert alert-success">
                    <strong>{{ Session::get('message')  }}</strong>
                  </div>
                @endif
                @if(Session::has('error'))
                  <div class="alert alert-danger">
                    <strong>{{ Session::get('error')  }}</strong>
                  </div>
                @endif


         <section id="configuration">
            <div class="row">
               <div class="col-12">
                  <div class="card rounded">
                     <div class="card-content collapse show">
                        <div class="card-body card-dashboard">
                           <h1>Profile</h1>
                           <form novalidate action="{{url('/customer/update-customer')}}" enctype="multipart/form-data" method="post">
                             @csrf
                             <input type="hidden" name="customer_id" value="{{$customer->id}}">
                             <input type="hidden" name="referer" value="c">

                           <div class="row">
                              <div class="col-md-10 col-sm-12">
                                 <div class="row">
                                    <div class="col-md-3 col-sm-12">
                                      <button type="button" style="width:50%; margin-top: 0;" name="file" class="uplon-btn" onclick="document.getElementById('upload').click()">
                                        <img src="{{$meta->personal_img ?? '/images/upload-img.jpg'}}" id="personal_img" class="img-full" alt=""></button>
                                        <input type="file"  accept="image/*" required onchange="readURL(this, 'personal_img')"  id="upload" name="personal_img">
                                    </div>
                                    <div class="col-md-8 col-sm-12"></div>
                                 </div>
                                 <br>
                                 <div class="form-group">
                                    <label for="timesheetinput1">Name</label>
                                    <div class="position-relative has-icon-left">
                                       <input type="text" value="{{$customer->name}}" id="timesheetinput1" class="form-control" name="name">
                                       <div class="form-control-position"> <i class="fa fa-user-circle"></i> </div>
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <label for="timesheetinput1">Gender</label>
                                    <div>
                                      <div class="d-inline-block custom-control custom-radio mr-1">
                                         <input type="radio" {{$meta->gender == 'male' ? 'checked' : ''}} class="custom-control-input" value="male" name="gender" id="radio1">
                                         <label class="custom-control-label" for="radio1">Male</label>
                                      </div>
                                      <div class="d-inline-block custom-control custom-radio mr-1">
                                         <input type="radio" {{$meta->gender == 'female' ? 'checked' : ''}} class="custom-control-input" value="female" name="gender" id="radio2">
                                         <label class="custom-control-label" for="radio2" checked="">Female</label>
                                      </div>
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <label for="timesheetinput1">DOB</label>
                                    <div class="position-relative has-icon-left">
                                       <input type="date" value="{{$meta->dob}}" id="timesheetinput1" class="form-control" name="dob">
                                       <div class="form-control-position"> <i class="fa fa-user"></i> </div>
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <label for="timesheetinput1">Email Address</label>
                                    <div class="position-relative has-icon-left">
                                         <input type="email" value="{{$customer->email}}" disabled id="email" class="form-control" name="email">
                                       <div class="form-control-position"> <i class="fa fa-envelope"></i> </div>
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <label for="timesheetinput1">Phone Number</label>
                                    <div class="position-relative has-icon-left">
                                       <input type="number" value="{{$meta->phone}}" id="timesheetinput1" class="form-control" name="number">
                                       <div class="form-control-position"> <i class="fa fa-phone"></i> </div>
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <label for="timesheetinput1">Address</label>
                                    <div class="position-relative has-icon-left">
                                       <input type="text" value="{{$meta->address}}" id="autocomplete" required onfocus="geolocate()"  class="form-control" name="address">
                                       <div class="form-control-position"> <i class="fa fa-map-marker"></i> </div>
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <label for="timesheetinput1">Country</label>
                                    <div class="position-relative has-icon-left">
                                       <input type="text" value="{{$meta->country}}" id="country" class="form-control" name="country">
                                       <div class="form-control-position"> <i class="fa fa-globe"></i> </div>
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <label for="timesheetinput1">City</label>
                                    <div class="position-relative has-icon-left">
                                        <input type="text" value="{{$meta->city}}" required class="form-control" id="locality" name="city" />
                                       <div class="form-control-position"> <i class="fa fa-map-marker"></i> </div>
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <label for="timesheetinput1">State</label>
                                    <div class="position-relative has-icon-left">
                                      <input type="text" value="{{$meta->state}}" id="administrative_area_level_1" class="form-control" name="state">
                                       <div class="form-control-position"> <i class="fa fa-globe"></i> </div>
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <label for="timesheetinput1">Zip Code</label>
                                    <div class="position-relative has-icon-left">
                                      <input required value="{{$meta->zipcode}}" type="text" class="form-control" id="postal_code" name="postal_code" />
                                       <div class="form-control-position"> <i class="fa fa-globe"></i> </div>
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <label for="timesheetinput1">Update Password or Leave Blank</label>
                                    <div class="position-relative has-icon-left">
                                       <input type="password"  class="form-control" name="password" />
                                       <div class="form-control-position"> <i class="fa fa-key"></i> </div>
                                    </div>
                                 </div>
                                 <div class="cntr-btnn-main">
                                    <button class="emply-contact-btn">Save Changes</button>
                                 </div>
                              </div>
                              <!--col end-->
                              <div class="col-md-2 col-sm-12"></div>
                           </div>
                           <!--row end-->

                            </form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </div>
   </div>
</div>

@section('js')
<script type="text/javascript">
  function readURL(input, target) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function(e) {
        $('#'+target).attr('src', e.target.result);
      }
      reader.readAsDataURL(input.files[0]);
    }
  }


</script>
@endsection

@endsection
