@extends('layouts.app')
@section('title','Customer')
@section('body-class','vertical-layout vertical-menu 2-columns fixed-navbar pace-done menu-expanded')
@section('css')
@endsection
@section('content')

<div class="app-content content">
    <div class="content-wrapper">
      <div class="content-body">

        @if(Session::has('message'))
          <div class="alert alert-success">
            <strong>{{ Session::get('message')  }}</strong>
          </div>
        @endif
        @if(Session::has('error'))
          <div class="alert alert-danger">
            <strong>{{ Session::get('error')  }}</strong>
          </div>
        @endif


        <!-- Basic form layout section start -->
         <section id="configuration">
          <div class="row">
            <div class="col-12">
              <div class="card rounded">
                <div class="card-content collapse show">
                     <div class="card-body card-dashboard">

                     <h1 class="pull-left">Customer</h1>
                     <a href="{{url('/customer/add-customer')}}" class="green-btn-project"><i class="fa fa-plus-circle"></i> Add Customer</a>

                     <div class="maain-tabble">
                      <table class="table table-striped table-bordered zero-configuration">
                      <thead>
                        <tr>
                          <th>Customer Name</th>
                          <th>Email Address</th>
                          <th>Phone</th>
                          <th>Gender</th>
                          <th>Address</th>
                         </tr>
                      </thead>
                      <tbody>
                        @foreach ($customers as $key => $cust)
                       <tr id="row{{$cust->id}}">
                          <td><img src="{{asset($cust->personal_img)}}" class="tab-img" alt=""> {{$cust->name}}</td>
                          <td><i class="fa fa-envelope" aria-hidden="true"></i><a>{{$cust->email}}</a></td>
                          <td>{{$cust->phone}}</td>
                          <td>{{$cust->gender}}</td>
                          <td>{{$cust->address}}</td>
                          <td>
                          <div class="btn-group mr-1 mb-1">
                            <button type="button" class="btn dropdown-toggle btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></button>
                            <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 21px, 0px); top: 0px; left: 0px; will-change: transform;">
                            <a class="dropdown-item" href="{{url('/customer/edit/'.$cust->id)}}"><i class="fa fa-pencil-square-o"></i>Edit</a>
                            <a class="dropdown-item" onclick="deleteCustomer({{$cust->id}})" href="#!"><i class="fa fa-eye-slash"></i>Delete</a>
                            </div>
                          </div>
                        </td>
                        </tr>
                        @endforeach

                      </tbody>
                    </table>
                    </div>
                    </div><!--card body end-->
                </div>
              </div>
            </div>
          </div>
        </section>
        <!-- // Basic form layout section end -->
      </div>
    </div>
  </div>


@section('js')
<script type="text/javascript">

function deleteCustomer(id) {
  toastr.warning("<br /><button type='button' value='yes'>Yes</button><button type='button'  value='no' >No</button>", 'Are you sure you want to delete this ?', {
      allowHtml: true,
      onclick: function(toast) {
          value = toast.target.value
          if (value == 'yes') {
              toastr.remove();
              $.ajax({
                  url: base_url + '/customer/delete-customer/',
                  type: 'post',
                  data: { id: id},
                  success: function(response) {
                      $("#row" + id).remove();
                  },
                  error: function(response) {
                      console.log(response);
                  }
              })
          } else {
              toastr.remove();
          }
      }

  })

}

</script>

@endsection


@endsection
