@extends('layouts.app')
@section('title','Add Customer')
@section('body-class','vertical-layout vertical-menu 2-columns fixed-navbar pace-done menu-expanded')
@section('css')
@endsection
@section('content')
<div class="app-content content">
   <div class="content-wrapper">
      <div class="content-body">
         <!-- Basic form layout section start -->
         <section id="configuration">
            <div class="row">
               <div class="col-12">
                  <div class="card rounded">
                     <div class="card-content collapse show">
                        <div class="card-body card-dashboard client-pro-main">
                           <h1>Add a Customer</h1>
                           <form action="{{url('/customer/add-customer')}}" enctype="multipart/form-data" method="post">
                             @csrf
                           <div class="admin-add-cstmr">
                              <div class="row">
                                 <div class="col-md-6 col-sm-12">
                                    <div class="row">
                                       <div class="col-md-3 col-sm-12">
                                          <button type="button" style="margin-top: 0;" name="file" class="uplon-btn" onclick="document.getElementById('upload').click()">
                                            <img src="/images/upload-img.jpg" id="personal_img" class="img-full" alt=""></button>
                                          <input type="file" accept="image/*" required onchange="readURL(this, 'personal_img')"  id="upload" name="personal_img">
                                       </div>
                                       <!--inner col end-->
                                       <div class="col-md-9 col-sm-12">
                                          <div class="form-group">
                                             <label for="timesheetinput1">Customer Name</label>
                                             <div class="position-relative has-icon-left">
                                                <input type="text" id="timesheetinput1" class="form-control" name="name">
                                                <div class="form-control-position"> <i class="fa fa-user"></i> </div>
                                             </div>
                                          </div>
                                          <div class="form-group">
                                             <label for="timesheetinput1">Gender</label>
                                             <div>
                                                <div class="d-inline-block custom-control custom-radio mr-1">
                                                   <input type="radio" class="custom-control-input" value="male" name="gender" id="radio1">
                                                   <label class="custom-control-label" for="radio1">Male</label>
                                                </div>
                                                <div class="d-inline-block custom-control custom-radio mr-1">
                                                   <input type="radio" class="custom-control-input" value="female" name="gender" id="radio2" checked="">
                                                   <label class="custom-control-label" for="radio2" checked="">Female</label>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <!--inner col end-->
                                    </div>
                                    <!--inner row end-->
                                 </div>
                                 <!--col end-->
                                 <div class="col-md-6 col-sm-12">
                                    <div class="form-group">
                                       <label for="timesheetinput1">Address * </label>
                                       <div class="position-relative has-icon-left">
                                          <input type="text" id="autocomplete" required onfocus="geolocate()"  class="form-control" name="address">
                                          <div class="form-control-position"> <i class="fa fa-map-marker"></i> </div>
                                       </div>
                                    </div>
                                    <div class="form-group">
                                       <label for="timesheetinput1">Country *</label>
                                       <div class="position-relative has-icon-left">
                                          <input type="text" id="country" class="form-control" name="country">
                                          <div class="form-control-position"> <i class="fa fa-globe"></i> </div>
                                       </div>
                                    </div>
                                 </div>
                                 <!--col end-->
                              </div>
                              <div class="row">
                                 <div class="col-md-6 col-sm-12">
                                   <div class="row">
                                   <div class="col-md-6 col-sm-12">
                                     <div class="form-group">
                                        <label for="timesheetinput1">Date of Birth</label>
                                        <div class="position-relative has-icon-left">
                                           <input type="date" id="timesheetinput1" class="form-control" name="dob">
                                           <div class="form-control-position"> <i class="fa fa-calendar"></i> </div>
                                        </div>
                                     </div>
                                   </div>
                                   <div class="col-md-6 col-sm-12">
                                     <div class="form-group">
                                        <label for="timesheetinput1">Phone Number</label>
                                        <div class="position-relative has-icon-left">
                                           <input type="number" id="timesheetinput1" class="form-control" name="number">
                                           <div class="form-control-position"> <i class="fa fa-phone"></i> </div>
                                        </div>
                                     </div>
                                   </div>
                                  </div>
                                 </div>
                                 <div class="col-md-6 col-sm-12">
                                   <div class="form-group">
                                      <label for="timesheetinput1">State *</label>
                                      <div class="position-relative has-icon-left">
                                         <input type="text" id="administrative_area_level_1" class="form-control" name="state">
                                         <div class="form-control-position"> <i class="fa fa-globe"></i> </div>
                                      </div>
                                   </div>
                                </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-6 col-sm-12">
                                    <div class="form-group">
                                       <label for="timesheetinput1">Email Address</label>
                                       <div class="position-relative has-icon-left">
                                          <input type="email" id="email" class="form-control" name="email">
                                          <div class="form-control-position"> <i class="fa fa-envelope"></i> </div>
                                       </div>
                                    </div>
                                    <div class="form-group">
                                       <label for="timesheetinput1">Password</label>
                                       <div class="position-relative has-icon-left">
                                          <input type="password" class="form-control" name="password">
                                          <div class="form-control-position"> <i class="fa fa-key"></i> </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-md-6 col-sm-12">
                                     <div class="form-group">
                                        <label for="timesheetinput1">City *</label>
                                        <div class="position-relative has-icon-left">
                                           <input type="text" required class="form-control" id="locality" name="city" />
                                           <div class="form-control-position"> <i class="fa fa-globe"></i> </div>
                                        </div>
                                     </div>
                                     <div class="form-group">
                                        <label for="timesheetinput1">ZipCode *</label>
                                        <div class="position-relative has-icon-left">
                                           <input required type="text" class="form-control" id="postal_code" name="postal_code" />
                                           <div class="form-control-position"> <i class="fa fa-globe"></i> </div>
                                        </div>
                                     </div>
                                 </div>

                                 <!--col end-->
                              </div>
                              <div class="cntr-btnn-main">
                                 <button type="submit">Save Changes</button>
                              </div>
                           </div>
                           <!--admin add custmr end-->
                         </form>

                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <!-- // Basic form layout section end -->
      </div>
   </div>
</div>


@section('js')
<script type="text/javascript">
  function readURL(input, target) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function(e) {
        $('#'+target).attr('src', e.target.result);
      }
      reader.readAsDataURL(input.files[0]);
    }
  }




</script>
@endsection

@endsection
