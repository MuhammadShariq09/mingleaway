@extends('layouts.app')
@section('title','Mingle Away - Register')
@section('body-class','bg-full-screen-image  pace-done')
@section('content')
<div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
      </div>
      <div class="content-body">
        <section class="flexbox-container">
          <div class="col-12 d-flex align-items-center justify-content-center">
            <div class="col-lg-4 col-md-10 col-10 box-shadow-2 p-0">
              <div class="card rad border-grey border-lighten-3 px-1 py-1 m-0">
                <div class="card-header border-0">
                  <div class="card-title text-center">
                    <img src="{{asset('images/logo-dark.png')}}"  class="img-fluid" alt="branding logo">
                  </div>

                </div>
                <div class="card-content logn-form">

                  <div class="card-body">
                    @if ($errors->has('name'))
                        <div class="alert alert-danger" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </div>
                    @elseif ($errors->has('dateofbirth'))
                        <div class="alert alert-danger" role="alert">
                            <strong>{{ $errors->first('dateofbirth') }}</strong>
                        </div>
                    @elseif ($errors->has('gender'))
                        <div class="alert alert-danger" role="alert">
                            <strong>{{ $errors->first('gender') }}</strong>
                        </div>
                    @elseif ($errors->has('phone'))
                        <div class="alert alert-danger" role="alert">
                            <strong>{{ $errors->first('phone') }}</strong>
                        </div>
                    @elseif ($errors->has('address'))
                        <div class="alert alert-danger" role="alert">
                            <strong>{{ $errors->first('address') }}</strong>
                        </div>
                    @elseif ($errors->has('email'))
                        <div class="alert alert-danger" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </div>
                    @elseif ($errors->has('password'))
                        <div class="alert alert-danger" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </div>
                    @endif

                    <form class="form-horizontal" action="{{ route('register') }}" method="post">
                      @csrf
                      <fieldset class="form-group position-relative has-icon-left">
                        <input type="text" class="form-control" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}"  placeholder="User Name"
                        required>
                        <div class="form-control-position">
                          <i class="fa fa-user-circle"></i>
                        </div>
                      </fieldset>

                      <div class="form-group">
                  <div>
                  <label class="login-form-lab-gendr" for="timesheetinput1">Gender</label>
                    <div class="d-inline-block custom-control custom-radio mr-1">
                      <input type="radio" value="male" class="custom-control-input" id="radio1" name="gender"
                      {{ old('gender') == 'male' ? 'checked' : '' }}>
                      <label class="custom-control-label" for="radio1">Male</label>
                    </div>
                    <div class="d-inline-block custom-control custom-radio mr-1">
                      <input type="radio" value="female" class="custom-control-input" id="radio2" name="gender"
                      {{ old('gender') == 'female' ? 'checked' : '' }} >
                      <label class="custom-control-label" for="radio2" checked="">Female</label>
                    </div>
                  </div>
                </div>

                <div class="form-group">

                  <div class="position-relative has-icon-left">
                   <input type="date" id="timesheetinput3" required value="{{ old('dateofbirth') }}" class="form-control" name="dateofbirth">
                    <div class="form-control-position"> <i class="fa fa-calendar"></i></div>
                  </div>
                </div>


                      <fieldset class="form-group position-relative has-icon-left">
                        <input type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Email Address"
                        required>
                        <div class="form-control-position">
                        <i class="fa fa-envelope"></i>
                        </div>
                      </fieldset>

                       <fieldset class="form-group position-relative has-icon-left">
                        <input type="number" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" value="{{ old('phone') }}"  placeholder="Phone Number"
                        required>
                        <div class="form-control-position">
                        <i class="fa fa-phone"></i>
                        </div>
                      </fieldset>

                      <fieldset class="form-group position-relative has-icon-left">
                        <input type="text" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" name="address" value="{{ old('address') }}"  placeholder="Address"
                        required>
                        <div class="form-control-position">
                        <i class="fa fa-map-marker"></i>
                        </div>
                      </fieldset>


                      <fieldset class="form-group position-relative has-icon-left">
                        <input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password"
                        required>
                        <div class="form-control-position">
                          <i class="fa fa-key"></i>
                        </div>
                      </fieldset>

                       <fieldset class="form-group position-relative has-icon-left">
                        <input type="password" class="form-control" id="user-password" name="password_confirmation" placeholder="Retype Password"
                        required>
                        <div class="form-control-position">
                          <i class="fa fa-key"></i>
                        </div>
                      </fieldset>

                      <button type="submit" class="btn btn-outline-primary btn-block"> Register </button>
                    </form>
                  </div>
                  <p class="card-subtitle line-on-side text-muted text-center font-small-3 mx-2">
                    <span>Already a User ? </span>
                  </p>
                  <div class="card-body">
                    <a href="{{route('login')}}" class="btn btn-outline-danger btn-block"> Login</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </div>

@endsection
