<div class="main-menu menu-fixed menu-light menu-accordion" data-scroll-to-active="true">
    <div class="main-menu-content ps-container ps-theme-dark ps-active-y" data-ps-id="d67116d8-f431-3fd7-558c-6370ee832809" style="height: 274px;">

@if(Auth::user()->type == 2)
<ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
    <li class="nav-item active">
      <a href="#"><i class="fa fa-info-circle"></i><span class="menu-title" data-i18n="">Overview</span></a></li>
    <li class="nav-item">
      <a href="{{route('employee')}}"><i class="fa fa-user"></i><span class="menu-title" data-i18n="">Employee</span></a></li>
    <li class="nav-item">
      <a href="{{route('customer')}}"><i class="fa fa-user-circle"></i><span class="menu-title" data-i18n="">Customer</span></a></li>
</ul>
@elseif(Auth::user()->type == 1)
<ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
    <li class="nav-item active">
      <a href="#"><i class="fa fa-info-circle"></i><span class="menu-title" data-i18n="">Overview</span></a></li>
    <li class="nav-item">
      <a href="{{route('employee')}}"><i class="fa fa-user"></i><span class="menu-title" data-i18n="">Employee</span></a></li>
    <li class="nav-item">
      <a href="{{route('customer')}}"><i class="fa fa-user-circle"></i><span class="menu-title" data-i18n="">Customer</span></a></li>
    <li class="nav-item">
      <a href="#"><i class="fa fa-money"></i><span class="menu-title" data-i18n="">Payments</span></a></li>
    <li class="nav-item">
      <a href="#"><i class="fa fa-th-large"></i><span class="menu-title" data-i18n="">Manage Sections</span></a></li>
    <li class="nav-item">
      <a href="#"><i class="fa fa-file"></i><span class="menu-title" data-i18n="">Report</span></a></li>
</ul>
@else
<ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
    <li class="nav-item active">
      <a href="{{route('customerProfile')}}"><i class="fa fa-info-circle"></i><span class="menu-title" data-i18n="">Profile</span></a></li>
    <li class="nav-item">
      <a href="{{route('listemployeeforCustomer')}}"><i class="fa fa-user"></i><span class="menu-title" data-i18n="">Employee</span></a></li>
    <li class="nav-item">
      <a href="#"><i class="fa fa-user-circle"></i><span class="menu-title" data-i18n="">Paylogs</span></a></li>
    <li class="nav-item">
      <a href="#"><i class="fa fa-money"></i><span class="menu-title" data-i18n="">Video Logs</span></a></li>
    <li class="nav-item">
      <a href="#"><i class="fa fa-th-large"></i><span class="menu-title" data-i18n="">Contact Admin</span></a></li>
</ul>
@endif
        <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px;">
            <div class="ps-scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div>
        </div>
        <div class="ps-scrollbar-y-rail" style="top: 0px; right: 3px; height: 274px;">
            <div class="ps-scrollbar-y" tabindex="0" style="top: 0px; height: 258px;"></div>
        </div>
    </div>
</div>
