<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use App\User;
use redirect;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::table('call_logs as logs')->orderBy('id','desc')->get();
        return view('home',['users'=>$data]);
    }

    public function logout(){
      DB::table('users')->where('id',Auth::user()->id)->update(['socket_id' => '']);
      Auth::logout();
      return redirect('/');
    }
}
