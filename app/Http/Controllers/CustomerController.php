<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Auth;
use Redirect;
use Session;
use DB;
use Mail;
use App\Mail\WelcomeUserEmail;
use  App\User;
use Crypt;

class CustomerController extends Controller
{

  public function index(){
    $customers = DB::table('users as u')
                  ->join('user_meta as meta','u.id','=','meta.user_id')
                  ->select('u.*', 'meta.personal_img', 'meta.phone', 'meta.gender', 'meta.address')
                  ->where('u.type',0)->get();
    return view('pages.customer.all',['customers' => $customers]);
  }

  public function updateProfile(Request $request){
    $customer = DB::table('users')->where('id',Auth::user()->id)->first();
    $meta = DB::table('user_meta')->where('user_id',Auth::user()->id)->first();
    return view('pages.customer.updateProfile',['customer' => $customer, 'meta' => $meta]);
  }

  public function listEmployees(Request $request){
    $employees = DB::table('users as u')
                  ->join('user_meta as meta','u.id','=','meta.user_id')
                  ->select('u.*', 'meta.personal_img', 'meta.gender', 'meta.avail_hrs_from', 'meta.avail_hrs_to')
                  ->where('u.type',1)->paginate(9);
    return view('pages.customer.listemployee',['employees' => $employees]);
  }

  public function addCustomer(Request $request){
      $user_id = DB::table('users')->insertGetId([
                  'name' => $request->name,
                  'email' => $request->email,
                  'password' => Hash::make($request->password)
              ]);
      $user_meta = array(
            'user_id' => $user_id,
            'gender' => $request->gender,
            'zipcode' => $request->postal_code,
            'address' => $request->address,
            'country' => $request->country,
            'state' => $request->state,
            'city' => $request->city,
            'phone' => $request->number,
            'dob' => $request->dob,
      );
      if($request->file('personal_img')){
          $file = $request->file('personal_img');
          $filename = $file->getClientOriginalName();
          $path = '/users/'.md5($user_id).'/';
          $destination = public_path($path);
          $file->move($destination,$filename);
          $user_image = $path.$filename;
          $user_meta['personal_img'] = $user_image;
      }

      DB::table('user_meta')->insert($user_meta);
      $user =  array(
            'name' => $request->name,
            'email' => $request->email,
      );
      Mail::to($request->email)->send(new WelcomeUserEmail($user,0));
      Session::flash('message','Customer is added successfully');
      return redirect('/customer');

  }

  public function updateCustomer(Request $request){

    if($request->referer)
        $user_id = Auth::user()->id;
    else
        $user_id = $request->customer_id;

    $usersarr=array(
        'name' => $request->name,
    );
    if($request->password)
      $usersarr['password'] = Hash::make($request->password);

    DB::table('users')->where('id', $user_id)->update($usersarr);
    $user_meta = array(
          'gender' => $request->gender,
          'zipcode' => $request->postal_code,
          'address' => $request->address,
          'country' => $request->country,
          'state' => $request->state,
          'city' => $request->city,
          'phone' => $request->number,
          'dob' => $request->dob,
    );
    if($request->file('personal_img')){
        $file = $request->file('personal_img');
        $filename = $file->getClientOriginalName();
        $path = '/users/'.md5($user_id).'/';
        $destination = public_path($path);
        $file->move($destination,$filename);
        $user_image = $path.$filename;
        $user_meta['personal_img'] = $user_image;
    }

    DB::table('user_meta')->where('user_id', $user_id)->update($user_meta);
    Session::flash('message','Customer is updated successfully');
    if($request->referer)
      return redirect('/update-my-profile');
    else
      return redirect('/customer');
  }

  public function subscribePackage (Request $request){
    $user = Auth::User();
    $user->packg_info_temp = $request->package;
    $user->save();
    $url = url('/video/video-chatting/'.Crypt::encryptString(Auth::user()->id));
    return response()->json(['status'=>200, 'msg'=>'Package is subscribed', 'url' => $url ]);
  }




  public function editCustomer($id){
      $customer = DB::table('users')->where('id',$id)->first();
      $meta = DB::table('user_meta')->where('user_id',$id)->first();
      return view('pages.customer.edit',['customer' => $customer, 'meta' => $meta]);
  }


  public function deleteCustomer(Request $request){
    DB::table('users')->where('id',$request->id)->delete();
    DB::table('user_meta')->where('user_id',$request->id)->delete();
    return response()->json(['status'=>200, 'msg'=>'Customer is deleted']);
  }

}
