<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Auth;
use Redirect;
use Session;
use DB;
use Mail;
use App\Mail\WelcomeUserEmail;
class EmployeeController extends Controller
{
    public function index(){
      $employees = DB::table('users as u')
                    ->join('user_meta as meta','u.id','=','meta.user_id')
                    ->select('u.*', 'meta.personal_img', 'meta.phone', 'meta.avail_hrs_from', 'meta.avail_hrs_to')
                    ->where('u.type',1)->get();
      return view('pages.employee.all',['employees' => $employees]);
    }

    public function employeeProfile($id){
      $employee = DB::table('users')->where('id',$id)->first();
      $meta = DB::table('user_meta')->where('user_id',$id)->first();
      $package = DB::table('package')->where('user_id',$id)->get();
      $images = DB::table('images')->where('user_id',$id)->get();
      return view('pages.employee.employeeProfile',['employee' => $employee, 'meta' => $meta, 'package' => $package, 'images' => $images]);

    }

    public function getEmployeeId(Request $request){

    }

    public function edit($id, Request $request){
      $request->session()->forget('employee_package');
      $employee = DB::table('users')->where('id',$id)->first();
      $meta = DB::table('user_meta')->where('user_id',$id)->first();
      $package = DB::table('package')->where('user_id',$id)->get();
      $images = DB::table('images')->where('user_id',$id)->get();

      foreach($package as $p){
        $request->session()->push('employee_package', array(
          'id' => md5(str_random(32)),
          'package_name' => $p->package_name,
          'package_time' => $p->package_time,
          'package_cost' => $p->package_cost,
        ));
      }

      return view('pages.employee.edit',['employee' => $employee, 'meta' => $meta, 'package' => $package, 'images' => $images]);

    }

    public function addEmployee(Request $request){
      $request->session()->forget('employee_package');
      return view('pages.employee.add');
    }

    public function addEmployeePackage(Request $request){
        $request->session()->push('employee_package', array(
          'id' => md5(round(microtime(true) * 1000)),
          'package_name' => $request->package_name,
          'package_time' => $request->package_time,
          'package_cost' => $request->package_cost,
        ));
        return response()->json(['status' =>200, 'msg' => 'Package has been added']);
    }

    public function removeEmployeeGalImage($id){
        DB::table('images')->where('id',$id)->delete();
        return response()->json(['status' =>200, 'msg' => 'Image is deleted']);
    }

    public function updateEmployee(Request $request){
      $user_id = $request->employee_id;
      $usersarr = array(
        'name' => $request->name
      );

      if($request->password){
        $usersarr['password'] = Hash::make($request->password);
      }
      $usersarr['updated_at'] = date('Y-m-d H:i:s');
      DB::table('users')->where('id',$user_id)->update($usersarr);

      $user_meta = array(
        'gender' => $request->gender,
        'stage_name' => $request->stagename,
        'consultant_id' => $request->consultant_id,
        'heritage' => $request->heritage,
        'haircolor' => $request->haircolor,
        'zipcode' => $request->postal_code,
        'address' => $request->address,
        'country' => $request->country,
        'state' => $request->state,
        'city' => $request->city,
        'avail_hrs_from' => $request->availablity_hour_from,
        'aboutsus' => $request->aboutus,
        'accountname' => $request->account_name,
        'accountnumber' => $request->account_number,
        'cvcnumber' => $request->account_cvc,
        'expirynumber' => $request->account_expdate,
        'avail_hrs_to' => $request->availablity_hour_to,
        'phone' => $request->number,
      );
      if($request->file('personal_img')){
          $file = $request->file('personal_img');
          $filename = $file->getClientOriginalName();
          $path = '/users/'.md5($user_id).'/';
          $destination = public_path($path);
          $file->move($destination,$filename);
          $user_image = $path.$filename;
          $user_meta['personal_img'] = $user_image;
      }
      if($request->file('resume')){
          $file = $request->file('resume');
          $filename = $file->getClientOriginalName();
          $path = '/users/'.md5($user_id).'/';
          $destination = public_path($path);
          $file->move($destination,$filename);
          $user_image = $path.$filename;
          $user_meta['resume'] = $user_image;
      }

      DB::table('user_meta')->where('user_id',$user_id)->update($user_meta);

      if(count($request->file('gallary_photos'))){
        foreach ($request->file('gallary_photos') as $key => $file) {
          $filename = $file->getClientOriginalName();
          $path = '/users/'.md5($user_id).'/gallery/';
          $destination = public_path($path);
          $file->move($destination,$filename);
          $user_image = $path.$filename;
          DB::table('images')->insert([
            'user_id' => $user_id,
            'path' => $user_image,
          ]);
        }
      }
      DB::table('package')->where('user_id',$user_id)->delete();
      foreach ($request->session()->get('employee_package') as $key => $pkg) {
          DB::table('package')->insert([
            'user_id' => $user_id,
            'package_name' => $pkg['package_name'],
            'package_time' => $pkg['package_time'],
            'package_cost' => $pkg['package_cost'],
          ]);
      }
      Session::flash('message','Employee is updated successfully');
      return redirect('/employee');
    }

    public function submitEmployee(Request $request){

        $user_id = DB::table('users')->insertGetId([
                  'name' => $request->name,
                  'email' => $request->email,
                  'password' => Hash::make($request->password),
                  'created_at' => date('Y-m-d H:i:s'),
                  'type' => 1
              ]);

        $user_meta = array(
          'user_id' => $user_id,
          'gender' => $request->gender,
          'stage_name' => $request->stagename,
          'consultant_id' => $request->consultant_id,
          'heritage' => $request->heritage,
          'haircolor' => $request->haircolor,
          'zipcode' => $request->postal_code,
          'address' => $request->address,
          'country' => $request->country,
          'state' => $request->state,
          'city' => $request->city,
          'avail_hrs_from' => $request->availablity_hour_from,
          'aboutsus' => $request->aboutus,
          'accountname' => $request->account_name,
          'accountnumber' => $request->account_number,
          'cvcnumber' => $request->account_cvc,
          'expirynumber' => $request->account_expdate,
          'avail_hrs_to' => $request->availablity_hour_to,
          'phone' => $request->number,
        );
        if($request->file('personal_img')){
            $file = $request->file('personal_img');
            $filename = $file->getClientOriginalName();
            $path = '/users/'.md5($user_id).'/';
            $destination = public_path($path);
            $file->move($destination,$filename);
            $user_image = $path.$filename;
            $user_meta['personal_img'] = $user_image;
        }
        if($request->file('resume')){
            $file = $request->file('resume');
            $filename = $file->getClientOriginalName();
            $path = '/users/'.md5($user_id).'/';
            $destination = public_path($path);
            $file->move($destination,$filename);
            $user_image = $path.$filename;
            $user_meta['resume'] = $user_image;
        }

        DB::table('user_meta')->insert($user_meta);

        if(count($request->file('gallary_photos'))){
          foreach ($request->file('gallary_photos') as $key => $file) {
            $filename = $file->getClientOriginalName();
            $path = '/users/'.md5($user_id).'/gallery/';
            $destination = public_path($path);
            $file->move($destination,$filename);
            $user_image = $path.$filename;
            DB::table('images')->insert([
              'user_id' => $user_id,
              'path' => $user_image,
            ]);
          }
        }
        if($request->session()->has('employee_package')){
          foreach ($request->session()->get('employee_package') as $key => $pkg) {
            DB::table('package')->insert([
              'user_id' => $user_id,
              'package_name' => $pkg['package_name'],
              'package_time' => $pkg['package_time'],
              'package_cost' => $pkg['package_cost'],
            ]);
          }
        }
       $user =  array(
              'name' => $request->name,
              'email' => $request->email,
        );
        Mail::to($request->email)->send(new WelcomeUserEmail($user,0));
        Session::flash('message','Employee is added successfully');
        return redirect('/employee');

    }

    public function deleteEmployee($id){
      DB::table('users')->where('id',$id)->delete();
      DB::table('user_meta')->where('user_id',$id)->delete();
      DB::table('images')->where('user_id',$id)->delete();
      DB::table('package')->where('user_id',$id)->delete();
    }



    function getCurrentEmployeePackage(Request $request){
      return $request->session()->get('employee_package');
    }

    function removeCurrentEmployeePackage($id, Request $request){
        $temp = array();
        foreach ($request->session()->get('employee_package') as $key => $value) {
          if($value['id'] != $id){
            array_push($temp, $value);
          }
        }
        $request->session()->forget('employee_package');
        $request->session()->put('employee_package',$temp);
    }

}
