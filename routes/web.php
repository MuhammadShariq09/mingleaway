<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');

//////////////////// ROUTES FOR ADMIN PANEL //////////////////

//Employee
Route::get('/employee','EmployeeController@index')->name('employee');
Route::get('/add-employee','EmployeeController@addEmployee')->name('add-employee');
Route::get('/employee/edit/{id}','EmployeeController@edit');
Route::get('/employee/remove-current-package/{id}','EmployeeController@removeCurrentEmployeePackage');
Route::get('/employee/delete-gallary-image/{id}','EmployeeController@removeEmployeeGalImage');
Route::get('/employee/get-current-employee-package','EmployeeController@getCurrentEmployeePackage');
Route::get('/employee/delete-employee/{id}','EmployeeController@deleteEmployee');
Route::post('/employee/update-employee','EmployeeController@updateEmployee')->name('updateEmployee');
Route::post('/employee/add-employee-package','EmployeeController@addEmployeePackage');
Route::post('/employee/add-emplpoyee','EmployeeController@submitEmployee')->name('addEmployee');

//customer
Route::view('/customer/add-customer','pages.customer.add');
Route::get('/customer','CustomerController@index')->name('customer');
Route::get('/customer/edit/{id}','CustomerController@editCustomer');
Route::get('/video/video-chatting/{token}','VideoController@index');
Route::post('/customer/add-customer','CustomerController@addCustomer');
Route::post('/customer/update-customer','CustomerController@updateCustomer');
Route::post('/customer/delete-customer','CustomerController@deleteCustomer');
Route::post('/customer/subscribe-package','CustomerController@subscribePackage');


///////////////////////// ROUTES FOR USER PANEL //////////////////////////
Route::get('/update-my-profile','CustomerController@updateProfile')->name('customerProfile');
Route::get('/list-employees','CustomerController@listEmployees')->name('listemployeeforCustomer');
Route::get('employee/info/{id}','EmployeeController@employeeProfile');

Route::get('/logout', 'HomeController@logout');


// to check if the chat used by site Host is authenticated or not
Route::post('/get-target-address', function(Request $request){
 return Crypt::decryptString(request()->id);
});
Route::post('/check-if-host-authenticated', function(){
    if (in_array($_POST['host'], config('authenticatedHost.host')))
        return 1;
    else
        return 0;
});
